package Vredenslack::Configuration::Files;

use strict;
use warnings;
use v5.12;

use Carp;
#use Cwd qw/abs_path/;
use File::Basename ;
use File::Copy qw/cp/;
use File::Path qw/make_path remove_tree/;
use File::Spec::Functions;

require Vredenslack::Configuration::Base;

use parent "Vredenslack::Configuration::Base";

sub new {
	my $class = shift;

	my $self = $class->SUPER::new(@_);

	croak 'no list of files provided in configuration' unless defined $self->{files};

	$self;
}

sub copy {
	my $self = shift;
	my ($src, $dst) = @_;

	foreach my $file (@{$self->{files}}) {
		my $_src = catfile($src, $file);
		my $_dst = catfile($dst, $file);
		my $_folder = catfile($dst, dirname ($file));

		if (-e $_src) {
			my $_folder = dirname ($_dst);

			#Vredenslack::Configuration::Manager::_log(3, '[dst:', $_dst, '][dst folder:', $_folder, ']');

			# check if the destination folder exists or not
			unless (-d $_folder) {
				Vredenslack::Configuration::Manager::_log(3, 'creating folder [', $_folder, ']');
				make_path $_folder or croak 'failed to make path [', $_folder, ']';
			}

			if (-f $_src) {
				Vredenslack::Configuration::Manager::_log(3, 'copying file [', $_src, '] to [', $_folder, ']');
				cp $_src, $_dst or croak 'failed to copy file [', $_src, '] to [', $_folder, ']';
			} elsif (-d $_src) {
				# TODO: copy folder
				Vredenslack::Configuration::Manager::_log(3, 'copying folder [', $_src, '] to [', $_folder, ']');
				if (system('cp', '-r', $_src, $_folder) != 0) {
					croak 'failed to copy folder [', $_src, '] to [', $_folder, ']';
				}
			} else {
				Vredenslack::Configuration::Manager::_log(2, '[', $_src, '] is neither a file nor a folder, skipping');
			}
		} else {
			Vredenslack::Configuration::Manager::_log(2, '[', $_src, '] does not exist, skipping');
		}
	}
}

1;

__END__

=head1 NAME

Vredenslack::Configuration::Files - files plugin for the configuration manager

=head1 DESCRIPTION

This module is part of the L<Vredenslack::Configuration> tool. It
implements the configuration based on file/folder copying.

=head1 PUBLIC METHODS

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013 JB Ribeiro.

This program is free software; you can redistribute
it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in the
LICENSE file included with this module.

=cut
