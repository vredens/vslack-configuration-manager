package Vredenslack::Configuration::Rsync;

use warnings;
use strict;
use v5.12;

use Carp;

use parent "Vredenslack::Configuration::Base";

sub new {
	my $class = shift;

	my $self = $class->SUPER::new(@_);

	croak 'no options provided in configuration' unless defined $self->{options};
	$self->{options}->{update} = 'true' unless defined $self->{options}->{update};

	$self;
}

sub copy {
	my $self = shift;
	my $src = shift;
	my $dst = shift;

	$src = $src . '/' unless $src =~ m!/$!;
	$dst = $dst . '/' unless $dst =~ m!/$!;

	my @opts = ('-aqz');
	for (qw/delete update/) {
		push @opts, "--$_" if defined $self->{options}->{$_} and $self->{options}->{$_} eq 'true';
	}

	if ($self->{options}->{file}) {
		my $_file = $self->{options}->{file};
		$_file = File::Spec->catfile($self->{'base-folder'}, $_file) unless File::Spec->file_name_is_absolute( $_file );
		croak 'could not find rsync file [', $_file, ']' unless -f $_file;
		push @opts, '--include-from=' . $_file;
	}

	my $opts = join(' ', @opts);
	Vredenslack::Configuration::Manager::_log(3, 'syncing from [', $src, '] to [', $dst, '] using options [', $opts, ']');
	`rsync $opts $src $dst`;
	croak 'failed to run rsync' if $?;
}

1;

__END__

=head1 NAME

Vredenslack::Configuration::Rsync - rsync plugin for the configuration manager

=head1 DESCRIPTION

This module is part of the L<Vredenslack::Configuration> tool. It
implements the configuration based on rsync file transfer.

B<NOTE>: this module requires the C<rsync> command to be available.

=head1 CONFIGURATION

	my $cfg = {
		'source' => '/path/to/source/folder',
		'type' => 'rsync',
		'options' => {
			'file' => 'rsync/mymod.rsync',
			'delete' => 0
		}
	};

or using YAML (assuming you already know the configuration file structure):

	mymod:
	  source: /path/to/source/folder
	  type: rsync
	  options:
	    file: rsync/mymod.rsync
	    delete: false

The C<file> option lets you specify a file to be used as the C<include-from>
option of RSync. The path is relative to the C<base-folder> configuration.
You can also provide an absolute path, although this is discouraged.

=head1 PUBLIC METHODS

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013 JB Ribeiro.

This program is free software; you can redistribute
it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in the
LICENSE file included with this module.

=cut
