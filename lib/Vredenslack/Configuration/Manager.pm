package Vredenslack::Configuration::Manager;

use warnings;
use strict;
use v5.12;

# debug
use Data::Dumper;

use Archive::Tar;
use Carp;
use Cwd;
use File::Basename;
use File::Copy qw/cp/;
use File::Find;
use File::Path qw/make_path remove_tree/;
use File::Spec::Functions;
use Module::Load;

our $VERSION = '0.1';
our $logger;

# to be use for the log system
use constant ERROR => 1;
use constant WARN  => 2;
use constant DEBUG => 3;

#use Vredenslack::Configuration::Base;
#use Vredenslack::Configuration::Files;

sub _log {
	&$logger (@_) if (defined $logger and ref $logger eq 'CODE');
}

sub set_logger {
	$logger = shift;
}

sub new {
	my $class = shift;
	my $cfg = shift;

	my $self = {};

	# validate the configuration for bare minimum
	croak 'no base-folder defined in the configuration' unless defined $cfg->{'base-folder'};
	croak 'you have no modules configured' unless defined $cfg->{configurations};

	# setup the base folder
	$self->{'base-folder'} = _parse_location($cfg->{'base-folder'});
	_locate_folder_and_create($self->{'base-folder'});

	# setup the backup folder
	$self->{'backup-folder'} = catdir ($self->{'base-folder'}, 'backups');
	_locate_folder_and_create($self->{'backup-folder'});

	$self->{configurations} = {};
	foreach my $k (keys %{$cfg->{configurations}}) {
		_log DEBUG, 'processing [', $k, '] configuration module';

		my $_tmp = $cfg->{configurations}->{$k};

		# load the module
		my $_class = 'Vredenslack::Configuration::'.(ucfirst $_tmp->{type});
		load $_class;

		# attach the base-folder to the module configuration
		$_tmp->{'base-folder'} = $self->{'base-folder'};

		# create instance
		$self->{configurations}->{$k} = new $_class($_tmp);
	}

	bless $self, $class;
}

sub list {
	my $self = shift;

	keys %{$self->{'configurations'}};
}

sub config {
	my $self = shift;
	my $cn = shift || croak 'no configuration provided';

	croak 'no configuration found for ', $cn unless defined $self->{'configurations'}->{$cn};
	my $cfg = $self->{'configurations'}->{$cn};

	my $src = _locate_folder(catfile($self->{'backup-folder'}, $cn));

	$cfg->config_from($src);
}

sub backup {
	my $self = shift;
	my $cn = shift || croak 'no configuration provided';

	croak 'no configuration found for ', $cn unless defined $self->{'configurations'}->{$cn};
	my $cfg = $self->{'configurations'}->{$cn};

	my $dst = _locate_folder_and_create(catfile($self->{'backup-folder'}, $cn));

	$cfg->backup_to($dst);

}

sub archive {
	my $self = shift;
	my $cn = shift;

	croak 'no configuration found for ', $cn unless defined $self->{'configurations'}->{$cn};
	my $cfg = $self->{'configurations'}->{$cn};

	my $dst = _locate_folder($self->{'backup-folder'} . '/' . $cn);

	my @t = localtime;
	my $fn = $cn . '-' . sprintf("%04d%02d%02d%02d%02d", $t[5] + 1900, $t[4] + 1, $t[3], $t[2], $t[1]) . '.tar.gz';

	my $cwd = getcwd;
	chdir $self->{'backup-folder'};
	unless (-f $fn) {
		_log(DEBUG, 'compressing ', $cn, ' into ', $fn);
		my $tar = Archive::Tar->new();
		find ({ 'wanted' => sub { $tar->add_files($_); }, 'no_chdir' => 1}, $cn);
		$tar->write($fn, COMPRESS_GZIP) or croak 'failed to create archive';
	} else {
		_log(WARN, 'archive [', $fn, '] already exists');
	}
	chdir $cwd;
}

sub restore {
	my $self = shift;
	my $cn = shift;
	my $ts = shift;

	croak 'invalid timestamp' unless defined $ts and $ts =~ /^\d{12}$/;

	croak 'no configuration found for ', $cn unless defined $self->{'configurations'}->{$cn};
	my $cfg = $self->{'configurations'}->{$cn};

	my $cwd = getcwd;
	chdir $self->{'backup-folder'};

	my $fn = $cn . '-' . $ts . '.tar.gz';
	if (! -f $fn) {
		_log(ERROR, 'could not find the archive ', $fn);
		croak 'no archive found';
	}

	my $dst = _locate_folder($cn);
	if (-d $dst) {
		remove_tree($dst) or croak 'could not remove the configuration folder';
	}

	my $tar = Archive::Tar->new;
	$tar->read($fn);
	$tar->extract();

	chdir $cwd;
}

sub export {
	my $self = shift;
	my $fn = shift || croak 'no file name to export to was provided';

	my $cwd = getcwd;

	$fn = catfile($cwd, $fn) unless file_name_is_absolute($fn);
	$fn .= '.tar.gz' unless $fn =~ /\.tar\.gz$/;
	croak 'file ', $fn, ' already exists, remove it first' if (-f $fn);

	chdir dirname $self->{'backup-folder'};
	unless (-f $fn) {
		_log(DEBUG, 'compressing [', $self->{'backup-folder'}, '] into [', $fn, ']');
		my $tar = Archive::Tar->new();
		find ({ 'wanted' => sub { $tar->add_files($_); }, 'no_chdir' => 1}, basename $self->{'backup-folder'});
		$tar->write($fn, COMPRESS_GZIP) or croak 'failed to write archive';
	}
	chdir $cwd;
}

######################################################################## static methods

#
# locates a given file. croaks if not found.
#
sub _locate_file {
	my $fn = shift;

	croak 'file not found: ', $fn unless -f $fn;

	$fn;
}

sub _locate_folder {
	my $loc = shift || croak 'location is not defined';

	_log (DEBUG, 'checking if [', $loc, '] exists');
	croak $loc, ' does not exist.' unless -d $loc;

	$loc;
}

sub _locate_folder_and_create {
	my $loc = shift || croak 'location is not defined';

	_log (DEBUG, 'checking if folder [', $loc, '] exists and creating it otherwise');
	unless (-d $loc) {
		make_path($loc) or croak 'could not create path ', $loc;
	}

	$loc;
}

sub _parse_location {
	my $loc = shift || croak 'location is not defined';
	my $create = shift;

	$loc =~ s/\$HOME\$/$ENV{HOME}/;

	$loc;
}

1;

__END__

=head1 NAME

Vredenslack::Configuration::Manager - a configuration manager for Linux
systems.

=head1 SYNOPSIS

	my $cfg = {
		'base-folder'  => '/home/myuser/.cm/',
		'configurations' => {
			'bash' => {
				'source' => '$HOME$',
				'type' => 'files',
				'list'  => qw/.bashrc .bash_profile .bash_ext .alias/
			},
			'geany' => {
				'source' => '$HOME$/.config/geany/',
				'type' => 'rsync',
				'options'  => {
					'file' => 'geany.rsync'
				}
			}
		}
	};

	my $cm = new Vredenslack::Configuration::Manager($cfg);

	my @configs = $cm->list;

	$cm->config('geany');
	$cm->backup('geany');

	# store a .tar.gz file in the archives folder. the file includes the
	# timestamp of when it was created
	$cm->archive('geany');

	# recove the archive generated in 2012-10-13 at 05:10. the recovery
	# is done into the backups folder and not directly into the system
	$cm->restore('geany', '201210130510');
	# apply the recovered configuration
	$cm->config('geany');

=head1 DESCRIPTION

This module combines typical file/folder synchronization tools with a
structured configuration in order to provide an easy way of creating
configuration backups as well as deploying configurations in order to
reconfigure an environment.

It aims to be compatible with all Linux distros and that's it. Behavior
on other Operative Systems is not supported and never will be. As a
matter of fact, I only conduct tests on Slackware distributions.

Requires I<rsync> to be installed.

=head1 METHODS

=head2 config ($configuration_name)

Copies configurations from your configuration folder to your system.

=head2 backup ($configuration_name)

Copies configurations from your system to your configuration folder.

=head2 archive ($configuration_name)

Archives a configuration in the configuration folder. B<ACHTUNG>: it is
not your system configuration it is only the latest backup you did.

=head2 restore ($configuration_name, $timestamp)

Restores an archive into the configuration folder. B<ACHTUNG>: it does
not restore the archive into the system, only the configuration folder,
replacing your current backup with the contents of the archive.

=head1 AUTHOR

JB Ribeiro (Vredens) - E<lt>vredens@gmail.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2013 JB Ribeiro.

This program is free software; you can redistribute
it and/or modify it under the same terms as Perl itself.

The full text of the license can be found in the
LICENSE file included with this module.

=cut
